﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcMyBlog.Models;

//using Microsoft.Web.Helpers;

using System.Web.Helpers;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

namespace MvcMyBlog.Controllers
{ 
    public class HomeController : Controller
    {
        private PostContext db = new PostContext();

        //
        // GET: /Post/

        public ViewResult Index()
        {
            var posts = db.Posts.Include(p => p.CategoryPost).OrderByDescending(c=>c.Date);
            return View(posts.ToList());
        }


        public ViewResult Categories(string catname)
        {
            var posts = from p in db.Posts
                         select p;

            if (!String.IsNullOrEmpty(catname))
            {
                posts = posts.Where(s => s.CategoryPost.Category==catname);
            }

            return View(posts.ToList());
        }


        public ViewResult CurrentUser(string username)
        {
            var posts = from p in db.Posts
                        select p;

            if (!String.IsNullOrEmpty(username))
            {
                posts = posts.Where(s => s.UserName == username);
            }

            return View(posts.ToList());
        }



        //
        // GET: /Post/Details/5

        public ViewResult Details(int id)
        {
            Post post = db.Posts.Find(id);
            return View(post);
        }

        //
        // GET: /Post/Create

        
       
        public ActionResult Create()
        {
            ViewBag.CategoryPostID = new SelectList(db.CategoryPosts, "CategoryPostID", "Category");
            ViewBag.UserName = User.Identity.Name.ToString();
            ViewBag.Date = DateTime.Now;
            return View();
        } 

        //
        // POST: /Post/Create

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(Post post)
        {
            if (ModelState.IsValid)
            {
                db.Posts.Add(post);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            ViewBag.CategoryPostID = new SelectList(db.CategoryPosts, "CategoryPostID", "Category", post.CategoryPostID);
            ViewBag.UserName = User.Identity.Name.ToString();
            return View(post);
        }
        
        //
        // GET: /Post/Edit/5
 
        public ActionResult Edit(int id)
        {
            Post post = db.Posts.Find(id);
            ViewBag.CategoryPostID = new SelectList(db.CategoryPosts, "CategoryPostID", "Category", post.CategoryPostID);
            return View(post);
        }

        //
        // POST: /Post/Edit/5

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(Post post)
        {
            if (ModelState.IsValid)
            {
                db.Entry(post).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CategoryPostID = new SelectList(db.CategoryPosts, "CategoryPostID", "Category", post.CategoryPostID);
            return View(post);
        }

        //
        // GET: /Post/Delete/5
 
        public ActionResult Delete(int id)
        {
            Post post = db.Posts.Find(id);
            return View(post);
        }

        //
        // POST: /Post/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            Post post = db.Posts.Find(id);
            db.Posts.Remove(post);
            db.SaveChanges();
            return RedirectToAction("Index");
        }




        public ActionResult Upload()
        {
            var image = WebImage.GetImageFromRequest();

            if (image != null)
            {
                /*if (image.Width > 500)
                {
                    image.Resize(500, ((500 * image.Height) / image.Width));
                }*/

                var filename = Path.GetFileName(image.FileName);
                image.Save(Path.Combine("../Content/_images", filename));
                filename = Path.Combine("~/Content/_images", filename);

                //model.ImageUrl = Url.Content(filename);

            }

            return View("Index");
        }


        public ActionResult SearchIndex(string title)
        {
            var posts = from p in db.Posts
                         select p;

            if (!String.IsNullOrEmpty(title))
            {
                posts = posts.Where(s => s.Title.Contains(title));
            }

            return View(posts);

        }


        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}