﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

using System.ComponentModel.DataAnnotations;

namespace MvcMyBlog.Models
{
    public class Post
    {

        public int PostID { get; set; }

        [Required(ErrorMessage = "Title is required")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Text is required")]
        public string Text { get; set; }


        [UIHint("UploadedImage")]
        public string ImageUrl { get; set; }

     
        public int CategoryPostID { get; set; }
        
        public virtual CategoryPost CategoryPost { get; set; }

        public DateTime Date { get; set; }

        public string Tags { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }

        public string UserName { get; set; }      

    }
}